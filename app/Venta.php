<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $fillable = [
        'persona_id',
        'user_id',
        'tipo_comprobante',
        'serie_comprobante',
        'num_comprobante',
        'fecha_hora',
        'impuesto',
        'total_venta',
        'estado'
    ];

    public function users(){
        $this->belongsTo('App\User');
    }

    public function persona(){
        $this->belongsTo('App\Persona');
    }

    public function detalleVenta(){
        $this->hasMany('App\DetalleVenta');
    }
}
