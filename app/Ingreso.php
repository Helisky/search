<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingreso extends Model
{
    protected $fillable = [
        'persona_id',
        'tipo_comprobante',
        'serie_comprobante',
        'num_comprobante',
        'fecha_hora',
        'impuesto',
        'estado'
    ];

    public function persona(){
        $this->belongsTo('App\Persona');
    }

    public function detallesIng(){
        $this->hasMany('App\DetalleIngreso');
    }
}
