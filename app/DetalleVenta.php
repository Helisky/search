<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleVenta extends Model
{
    protected $fillable = [
        'venta_id',
        'articulo_id',
        'cantidad',
        'precio_venta',
        'descuento'
    ];

    public function articulo(){
        $this->belongsTo('App\Articulo');
    }

    public function venta(){
        $this->belongsTo('App\Venta');
    }
}
