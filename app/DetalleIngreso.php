<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleIngreso extends Model
{
    protected $fillable = [
        'ingreso_id',
        'articulo_id',
        'cantidad',
        'precio_compra',
        'precio_venta',
    ];

    public function ingreso(){
        $this->belongsTo('App\Ingreso');
    }

    public function articulo(){
        $this->belongsTo('App\Articulo');
    }
}
