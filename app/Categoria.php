<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = ['nombre', 'desc', 'condicion'];

    public function articulos(){
        return $this->hasMany('App\Articulos');
    }
}
