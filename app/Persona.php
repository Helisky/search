<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = [
        'tipo_persona',
        'nombre',
        'tipo_documento',
        'direccion',
        'telefono',
        'email'
    ];
   public function ingresos(){
       $this->hasMany('App\Ingreso');
   }
   
   public function ventas(){
       $this->hasMany('App\Venta');
   }
}
