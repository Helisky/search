<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoria = [
            [
                'nombre' => 'Herramienta',
                'desc'   => 'Herramientas',
                'condicion' => 1
            ],
            [
                'nombre' => 'Herramienta',
                'desc'   => 'Herramientas',
                'condicion' => 1
            ],
            [
                'nombre' => 'Herramienta',
                'desc'   => 'Herramientas',
                'condicion' => 1
            ],
            [
                'nombre' => 'Herramienta',
                'desc'   => 'Herramientas',
                'condicion' => 1
            ],
            [
                'nombre' => 'Herramienta',
                'desc'   => 'Herramientas',
                'condicion' => 1
            ],
            [
                'nombre' => 'Bebidas',
                'desc'   => 'Bebidas',
                'condicion' => 1
            ],
            [
                'nombre' => 'Alimento',
                'desc'   => 'Alimentos',
                'condicion' => 1
            ],
            [
                'nombre' => 'Herramienta',
                'desc'   => 'Herramientas',
                'condicion' => 1
            ],
            [
                'nombre' => 'Tuberia',
                'desc'   => 'Tuberias',
                'condicion' => 1
            ],
            [
                'nombre' => 'Linea Blanca',
                'desc'   => 'Lineas',
                'condicion' => 1
            ]
            ];
            DB::table('categorias')->insert($categoria);
    }
}
