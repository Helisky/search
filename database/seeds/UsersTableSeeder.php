<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
            [
               'name' => 'Lucrecia',
               'email' => 'menzmo77@gmail.com',
               'password' => bcrypt('12345678') 
            ],
            [
                'name' => 'Marina',
                'email' => 'menzmo@gmail.com',
                'password' => bcrypt('12345678') 
             ],
             [
                'name' => 'Lucia',
                'email' => 'menzmo7@gmail.com',
                'password' => bcrypt('12345678') 
             ],
            ];
            DB::table('users')->insert($usuarios);
    }
}
