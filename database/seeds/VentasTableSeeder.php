<?php

use Illuminate\Database\Seeder;

class VentasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ventas = [
            [
                'persona_id'        =>1,
                'user_id'           =>1,
                'tipo_comprobante'  =>'Recibo',
                'serie_comprobante' =>'B',
                'num_comprobante'   =>'1',
                'fecha_hora'        =>'2000-05-07 13:00:00',
                'impuesto'          =>'18.5',
                'total_venta'       =>'45.00',
                'estado'            =>1
            ],
            [
                'persona_id'        =>2,
                'user_id'           =>1,
                'tipo_comprobante'  =>'Recibo',
                'serie_comprobante' =>'B',
                'num_comprobante'   =>'1',
                'fecha_hora'        =>'2000-05-07 13:00:00',
                'impuesto'          =>'18.5',
                'total_venta'       =>'45.00',
                'estado'            =>1
            ],
            [
                'persona_id'        =>2,
                'user_id'           =>1,
                'tipo_comprobante'  =>'Recibo',
                'serie_comprobante' =>'B',
                'num_comprobante'   =>'1',
                'fecha_hora'        =>'2000-05-07 13:00:00',
                'impuesto'          =>'18.5',
                'total_venta'       =>'45.00',
                'estado'            =>1
            ],
            [
                'persona_id'        =>2,
                'user_id'           =>1,
                'tipo_comprobante'  =>'Recibo',
                'serie_comprobante' =>'B',
                'num_comprobante'   =>'1',
                'fecha_hora'        =>'2000-05-07 13:00:00',
                'impuesto'          =>'18.5',
                'total_venta'       =>'45.00',
                'estado'            =>1
            ],
        ];
        DB::table('ventas')->insert($ventas);
    }
}
