<?php

use Illuminate\Database\Seeder;

class DetalleVentasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $detalle = [
            [
                'venta_id'      =>1,
                'articulo_id'   =>1,
                'cantidad'      =>2,
                'precio_venta'  =>56,
                'descuento'     =>0.10
            ],
            [
                'venta_id'      =>2,
                'articulo_id'   =>1,
                'cantidad'      =>2,
                'precio_venta'  =>56,
                'descuento'     =>0.10
            ],
            [
                'venta_id'      =>3,
                'articulo_id'   =>1,
                'cantidad'      =>2,
                'precio_venta'  =>56,
                'descuento'     =>0.10
            ],
            [
                'venta_id'      =>4,
                'articulo_id'   =>1,
                'cantidad'      =>2,
                'precio_venta'  =>56,
                'descuento'     =>0.10
            ],
        ];
        DB::table('detalle_ventas')->insert($detalle);
    }
}
