<?php

use Illuminate\Database\Seeder;

class PersonasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $personas = [
            [
                'tipo_persona'      => 'Cliente',
                'nombre'            => 'Julian',
                'tipo_documento'    => 'Comprobante',
                'num_documento'     => '1',
                'direccion'         => 'Colinas del Bosque',
                'telefono'          => '45125263',
                'email'             => 'menzmo77@gmail.com'             
            ],

            [
                'tipo_persona'      => 'Proveedor',
                'nombre'            => 'Oscar',
                'tipo_documento'    => 'Comprobante',
                'num_documento'     => '1',
                'direccion'         => 'Colinas del Bosque',
                'telefono'          => '45125263',
                'email'             => 'menzmo77@gmail.com'             
            ],

            [
                'tipo_persona'      => 'Cliente',
                'nombre'            => 'Drake',
                'tipo_documento'    => 'Comprobante',
                'num_documento'     => '2',
                'direccion'         => 'Colinas del Bosque',
                'telefono'          => '45125263',
                'email'             => 'menzmo77@gmail.com'             
            ],

            [
                'tipo_persona'      => 'Empleado',
                'nombre'            => 'Juliana',
                'tipo_documento'    => 'Comprobante',
                'num_documento'     => '1',
                'direccion'         => 'Colinas del Bosque',
                'telefono'          => '45125263',
                'email'             => 'menzmo77@gmail.com'             
            ],
        ];
        DB::table('personas')->insert($personas);
    }
}
