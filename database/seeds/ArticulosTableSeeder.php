<?php

use Illuminate\Database\Seeder;

class ArticulosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articulos = [
            [
                'categoria_id' => 1,
                'codigo'       => 'ART001',
                'nombre'       =>'Cemento',
                'stock'        =>34,
                'estado'       =>1
            ],
            [
                'categoria_id' => 2,
                'codigo'       => 'ART002',
                'nombre'       =>'PVC',
                'stock'        =>14,
                'estado'       =>1
            ],
            [
                'categoria_id' => 3,
                'codigo'       => 'ART003',
                'nombre'       =>'Harina',
                'stock'        =>54,
                'estado'       =>1
            ],
            [
                'categoria_id' => 1,
                'codigo'       => 'ART004',
                'nombre'       =>'Leche',
                'stock'        =>74,
                'estado'       =>1
            ],
        ];
        DB::table('articulos')->insert($articulos);
    }
}
