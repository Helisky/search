<?php

use Illuminate\Database\Seeder;

class IngresosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ingresos = [
            [
                'persona_id'            => 1,
                'tipo_comprobante'      =>'Recibo',
                'serie_comprobante'     =>'A',
                'num_comprobante'       =>'1',
                'fecha_hora'            =>'2019-04-05 10:34:00',
                'impuesto'              =>'45.52',
                'estado'                =>'1'
            ],

            [
                'persona_id'            => 2,
                'tipo_comprobante'      =>'Recibo',
                'serie_comprobante'     =>'A',
                'num_comprobante'       =>'1',
                'fecha_hora'            =>'2019-04-05 10:34:00',
                'impuesto'              =>'45.52',
                'estado'                =>'1'
            ],

            [
                'persona_id'            => 3,
                'tipo_comprobante'      =>'Recibo',
                'serie_comprobante'     =>'A',
                'num_comprobante'       =>'1',
                'fecha_hora'            =>'2019-04-05 10:34:00',
                'impuesto'              =>'45.52',
                'estado'                =>'1'
            ],

            [
                'persona_id'            => 2,
                'tipo_comprobante'      =>'Recibo',
                'serie_comprobante'     =>'A',
                'num_comprobante'       =>'1',
                'fecha_hora'            =>'2019-04-05 10:34:00',
                'impuesto'              =>'45.52',
                'estado'                =>'1'
            ],
        ];
        DB::table('ingresos')->insert($ingresos);
    }
}
