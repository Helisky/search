<?php

use Illuminate\Database\Seeder;

class DetalleIngresosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datalle = [
            [
                'ingreso_id'        =>1,
                'articulo_id'       =>1,
                'cantidad'          =>45,
                'precio_compra'     =>10.25,
                'precio_venta'      =>14.00
            ],
            [
                'ingreso_id'        =>2,
                'articulo_id'       =>2,
                'cantidad'          =>45,
                'precio_compra'     =>10.25,
                'precio_venta'      =>14.00
            ],
            [
                'ingreso_id'        =>1,
                'articulo_id'       =>3,
                'cantidad'          =>45,
                'precio_compra'     =>10.25,
                'precio_venta'      =>14.00
            ],
            [
                'ingreso_id'        =>1,
                'articulo_id'       =>4,
                'cantidad'          =>45,
                'precio_compra'     =>10.25,
                'precio_venta'      =>14.00
            ],
        ];
    }
}
