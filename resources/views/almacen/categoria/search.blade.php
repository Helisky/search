{{-- {{ Form::open(array('url'=>'almacen/categoria', 'method'=>'GET','autocomplete'=>'off', 'role'=>'search')) }} --}}
<form action="/almacen/categoria" method="get" autocomplete="off" role="search">
@method('GET')
<div class="form-group">
    <div class="input-group">
        <input type="text" class="form-control" name="searchText" placeholder="Buscar" value="{{ $searchText }}">
        <span class="input-group-btn">
            <button>Buscar</button>
        </span>
    </div>
</div>
</form>
{{-- {{ Form::close() }} --}}