<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="row">
        <div class="col-lg-8 col-md-8">
            <h3>Listado de categorias <a href="categoria/create"><button>Nuevo</button></a></h3>
            @include('almacen.categoria.search') 
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-striped table-condensed table-hover">
                    <thead>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Opciones</th>
                    </thead>
                    @foreach ($categorias as $cat)
                        
                    <tr>
                       <td>{{ $cat->id }}</td> 
                       <td>{{ $cat->nombre }}</td> 
                       <td>{{ $cat->desc }}</td> 
                       <td>
                           <a href=""><button class="btn btn-info">Editar</button></a>
                           <a href=""><button class="btn btn-danger">Eliminar</button></a>
                        </td> 
                    </tr>
                    @endforeach
                </table>
            </div>
            {{ $categorias->render() }}
        </div>
    </div>
</body>
</html>